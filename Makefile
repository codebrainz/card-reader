CC := avr-gcc
OBJCOPY := avr-objcopy
AVRDUDE := avrdude

MCU := atmega328p
CLOCK := 16000000UL
PORT := /dev/ttyUSB0
BAUD := 57600

MON_PORT := $(PORT)
MON_BAUD := 9600

CFLAGS := $(CPPFLAGS) $(CFLAGS) \
	-DF_CPU=$(CLOCK) -mmcu=$(MCU) \
	-Os -flto -std=c11 \
	-Wall -Wextra -Werror
LDFLAGS := $(LDFLAGS)

SOURCES := main.c rbuf.c reader.c timer.c usart.c
OBJECTS := $(SOURCES:.c=.o)
DEPENDS := $(SOURCES:.c=.d)

all: card-reader.hex

clean:
	$(RM) *.bin *.d *.hex *.o

card-reader.hex: card-reader.bin
	$(OBJCOPY) $(strip -O ihex -R .eeprom card-reader.bin $@)

card-reader.bin: $(OBJECTS)
	$(CC) $(strip $(CFLAGS) -o $@ $(OBJECTS) $(LDFLAGS))

.c.o:
	$(CC) $(strip $(CFLAGS) -c -MMD -o $@ $<)

upload: card-reader.hex
	$(AVRDUDE) -F -V -c arduino -p $(MCU) -P $(PORT) -b $(BAUD) -U flash:w:card-reader.hex

monitor: upload
	@echo 'Press <Ctrl>A, <Ctrl>Q to exit...'
	@picocom --baud $(MON_BAUD) $(MON_PORT)

#	@stty -F $(MON_PORT) speed $(MON_BAUD) cs8 -cstopb -parenb 1>/dev/null
#	@echo 'Waiting for bitstreams...'
#	@cat $(PORT)

-include $(DEPENDS)

.PHONY: all clean monitor upload
