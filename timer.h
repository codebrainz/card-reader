#ifndef TIMER_H
#define TIMER_H

typedef void (*timer_callback_t)(void);

void timer1_init(float timeout_sec, timer_callback_t cb);
void timer1_start(void);
void timer1_stop(void);
void timer1_reset(void);

#endif // TIMER_H
