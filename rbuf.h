#ifndef BUFFER_H
#define BUFFER_H

#include <stdbool.h>
#include <stdint.h>

#define RBUF_SIZE 128

typedef struct
{
  uint8_t rd;
  uint8_t wr;
  uint8_t buf[RBUF_SIZE];
} rbuf_t;

void rbuf_init(volatile rbuf_t *buf);
bool rbuf_push(volatile rbuf_t *buf, uint8_t data);
bool rbuf_pop(volatile rbuf_t *buf, uint8_t *data);
bool rbuf_peek(const volatile rbuf_t *buf, uint8_t *data);

#endif // BUFFER_H
