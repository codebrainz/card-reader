#include "reader.h"
#include "usart.h"

#include <avr/io.h>
#include <avr/interrupt.h>

static void on_card_read(const char *bits, int len)
{
  usart_puts("C:");
  for (int i = 0; i < len; i++)
    usart_putc(bits[i] + 48);
  usart_puts("\r\n");
}

static void on_card_present(bool is_present)
{
  if (is_present)
    usart_put_line("P:CARD PRESENT");
  else
    usart_put_line("P:CARD NOT PRESENT");
}

static inline void toggle_output(reader_output_t op, char num, const char *name)
{
  reader_toggle_output(op);
  usart_putc(num);
  usart_putc(':');
  usart_puts(name);
  usart_putc(' ');
  if (reader_get_output(op))
    usart_put_line("ON");
  else
    usart_put_line("OFF");
}

int main()
{
  cli();
  usart_init(USART_BAUD_DEFAULT);
  reader_init(on_card_read, on_card_present);
  sei();

  while (1)
  {
    char ch = 0;
    if (usart_getc(&ch))
    {
      switch (ch)
      {
      case 'v':
      case 'V':
        usart_put_line("V:0.1");
        break;
      case '1':
        toggle_output(READER_OUTPUT_LED1, ch, "GREEN LED");
        break;
      case '2':
        toggle_output(READER_OUTPUT_LED2, ch, "RED LED");
        break;
      case '3':
        toggle_output(READER_OUTPUT_BUZZER, ch, "BUZZER");
        break;
      case '4':
        toggle_output(READER_OUTPUT_HOLD, ch, "HOLD");
        break;
      default:
        //usart_putc(ch);
        break;
      }
    }
  }

  return 0;
}
