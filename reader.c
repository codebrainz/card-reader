#include "reader.h"
#include "timer.h"
#include "usart.h"

#include <avr/io.h>
#include <avr/interrupt.h>

static volatile reader_card_read_callback_t rdr_cr_cb;
static volatile reader_card_present_callback_t rdr_cp_cb;
static volatile uint8_t bits[256] = {0};
static volatile uint8_t bit_count = 0;
static bool card_present = false;
static int initial_led_blink = 0;

static void on_timer_expired(void)
{
  timer1_stop();
  if (initial_led_blink == 1 || initial_led_blink == 3)
  {
    PORTC |= 1 << PORTC0;
    PORTC |= 1 << PORTC1;
    PORTC |= 1 << PORTC2;
    PORTC |= 1 << PORTC3;
    PORTC |= 1 << PORTC4;
    if (initial_led_blink == 1)
      initial_led_blink = 2;
    else
      initial_led_blink = 4;
    timer1_start();
  }
  else if (initial_led_blink == 2 || initial_led_blink == 4)
  {
    PORTC &= ~(1 << PORTC0);
    PORTC &= ~(1 << PORTC1);
    PORTC &= ~(1 << PORTC2);
    PORTC &= ~(1 << PORTC3);
    PORTC &= ~(1 << PORTC4);
    if (initial_led_blink == 2)
    {
      initial_led_blink = 3;
      timer1_start();
    }
    else
    {
      initial_led_blink = 0;
    }
  }
  else
  {
    rdr_cr_cb((const char *)bits, (int)bit_count);
    bit_count = 0;
    PORTC &= ~(1 << PORTC0);
  }
}

void reader_init(reader_card_read_callback_t cr_cb, reader_card_present_callback_t cp_cb)
{
  rdr_cr_cb = cr_cb;
  rdr_cp_cb = cp_cb;
  bit_count = 0;
  card_present = false;

  // Initialize the timeout timer
  timer1_init(0.25f, on_timer_expired);

  // Data 0
  DDRD &= ~(1 << DDD2); // make PD2 an input
  PORTD |= 1 << PORTD2; // enable pullup
  EICRA |= 1 << ISC11;  // trigger on falling edge
  EIMSK |= 1 << INT0;   // enable INT0

  // Data 1
  DDRD &= ~(1 << DDD3); // make PD3 an input
  PORTD |= 1 << PORTD3; // enable pullup
  EICRA |= 1 << ISC01;  // trigger on falling edge
  EIMSK |= 1 << INT1;   // enable INT1

  // Card Present
  DDRB &= ~(1 << DDB0);  // make PB0 an input
  PORTB |= 1 << PORTB0;  // enable pullup
  PCICR |= 1 << PCIE0;   // enable pin-change interrupt
  PCMSK0 |= 1 << PCINT0; // enable pin-change interrupt on PCINT0 (PB0)

  // LED 1
  DDRD |= 1 << DDD4;       // make PD4 an output
  PORTD &= ~(1 << PORTD4); // set to off

  // LED 2
  DDRD |= 1 << DDD5;       // make PD5 an output
  PORTD &= ~(1 << PORTD5); // set to off

  // Buzzer
  DDRD |= 1 << DDD6;       // make PD6 an output
  PORTD &= ~(1 << PORTD6); // set to off

  // Hold
  DDRD |= 1 << DDD7;       // make PD7 an output
  PORTD &= ~(1 << PORTD7); // set to off

  // RX/TX LED
  DDRC |= 1 << DDC0;       // make PC0 an output
  PORTC &= ~(1 << PORTC0); // set to off

  // Green LED
  DDRC |= 1 << DDC1;       // make PC1 an output
  PORTC &= ~(1 << PORTC1); // set to off

  // Red LED
  DDRC |= 1 << DDC2;       // make PC2 an output
  PORTC &= ~(1 << PORTC2); // set to off

  // Yellow LED
  DDRC |= 1 << DDC3;       // make PC3 an output
  PORTC &= ~(1 << PORTC3); // set to off

  // Blue LED
  DDRC |= 1 << DDC4;       // make PC4 an output
  PORTC &= ~(1 << PORTC4); // set to off

  initial_led_blink = 1;
  timer1_start();
}

bool reader_get_output(reader_output_t op)
{
  int value = 0;
  if (op & READER_OUTPUT_LED1 && (PORTD & (1 << PORTD4)))
    value++;
  if (op & READER_OUTPUT_LED2 && (PORTD & (1 << PORTD5)))
    value++;
  if (op & READER_OUTPUT_BUZZER && (PORTD & (1 << PORTD6)))
    value++;
  if (op & READER_OUTPUT_HOLD && (PORTD & (1 << PORTD7)))
    value++;
  return value > 0;
}

void reader_set_output(reader_output_t op)
{
  if (op & READER_OUTPUT_LED1)
  {
    PORTD |= 1 << PORTD4;
    PORTC |= 1 << PORTC1;
  }
  if (op & READER_OUTPUT_LED2)
  {
    PORTD |= 1 << PORTD5;
    PORTC |= 1 << PORTC2;
  }
  if (op & READER_OUTPUT_BUZZER)
  {
    PORTD |= 1 << PORTD6;
    PORTC |= 1 << PORTC3;
  }
  if (op & READER_OUTPUT_HOLD)
  {
    PORTD |= 1 << PORTD7;
    PORTC |= 1 << PORTC4;
  }
}

void reader_clear_output(reader_output_t op)
{
  if (op & READER_OUTPUT_LED1)
  {
    PORTD &= ~(1 << PORTD4);
    PORTC &= ~(1 << PORTC1);
  }
  if (op & READER_OUTPUT_LED2)
  {
    PORTD &= ~(1 << PORTD5);
    PORTC &= ~(1 << PORTC2);
  }
  if (op & READER_OUTPUT_BUZZER)
  {
    PORTD &= ~(1 << PORTD6);
    PORTC &= ~(1 << PORTC3);
  }
  if (op & READER_OUTPUT_HOLD)
  {
    PORTD &= ~(1 << PORTD7);
    PORTC &= ~(1 << PORTC4);
  }
}

void reader_toggle_output(reader_output_t op)
{
  if (op & READER_OUTPUT_LED1)
  {
    PORTD ^= 1 << PORTD4;
    PORTC ^= 1 << PORTC1;
  }
  if (op & READER_OUTPUT_LED2)
  {
    PORTD ^= 1 << PORTD5;
    PORTC ^= 1 << PORTC2;
  }
  if (op & READER_OUTPUT_BUZZER)
  {
    PORTD ^= 1 << PORTD6;
    PORTC ^= 1 << PORTC3;
  }
  if (op & READER_OUTPUT_HOLD)
  {
    PORTD ^= 1 << PORTD7;
    PORTC ^= 1 << PORTC4;
  }
}

ISR(INT0_vect)
{
  // PD2 is low, make sure PD3 is high
  if (PIND & (1 << PIND3))
    bits[bit_count++] = 0;
  PORTC |= 1 << PORTC0;
  timer1_start();
}

ISR(INT1_vect)
{
  // PD3 is low, make sure PD2 is high
  if (PIND & (1 << PIND2))
    bits[bit_count++] = 1;
  PORTC |= 1 << PORTC0;
  timer1_start();
}

ISR(PCINT0_vect)
{
  const bool new_state = !(PINB & (1 << PINB0));
  if (new_state != card_present)
  {
    // if PB0 is low, card is present
    card_present = new_state;
    rdr_cp_cb(new_state);
  }
}
