#include "timer.h"

#include <avr/io.h>
#include <avr/interrupt.h>

static volatile timer_callback_t tmr1_cb;

void timer1_init(float timeout_sec, timer_callback_t cb)
{
  // save the callback
  tmr1_cb = cb;
  // set time interval
  const float fcpu = (float)F_CPU;
  const float fprescaler = 256.0f;
  const float fvalue = ((fcpu / fprescaler) * timeout_sec) - 1.0f;
  const uint16_t value = (uint16_t)fvalue;
  OCR1AH = (value >> 8) & 0xFF;
  OCR1AL = value & 0xFF;
  // set CTC mode
  TCCR1B |= 1 << WGM12;
}

void timer1_start(void)
{
  // set the prescaler to 256
  TCCR1B |= 1 << CS12;
  // enable the timer interrupt
  TIMSK1 |= 1 << OCIE1A;
  // reset the counter value
  TCNT1H = 0;
  TCNT1L = 0;
}

void timer1_stop(void)
{
  // set prescaler to none (timer stopped)
  TCCR1B &= ~(1 << CS12);
  // disable the timer interrupt
  TIMSK1 &= ~(1 << OCIE1A);
}

void timer1_reset(void)
{
  // reset the counter value
  TCNT1H = 0;
  TCNT1L = 0;
}

ISR(TIMER1_COMPA_vect)
{
  tmr1_cb();
}
