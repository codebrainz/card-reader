#ifndef READER_H
#define READER_H

#include <stdbool.h>
#include <stdint.h>

typedef enum
{
  READER_OUTPUT_LED1 = 0b0001,
  READER_OUTPUT_LED2 = 0b0010,
  READER_OUTPUT_BUZZER = 0b0100,
  READER_OUTPUT_HOLD = 0b1000,
} reader_output_t;

typedef void (*reader_card_read_callback_t)(const char *, int);
typedef void (*reader_card_present_callback_t)(bool);

void reader_init(reader_card_read_callback_t cr_cb, reader_card_present_callback_t cp_cb);
bool reader_get_output(reader_output_t op);
void reader_set_output(reader_output_t op);
void reader_clear_output(reader_output_t op);
void reader_toggle_output(reader_output_t op);

#endif // READER_H
