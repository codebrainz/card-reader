#include "rbuf.h"

void rbuf_init(volatile rbuf_t *buf)
{
  buf->rd = buf->wr = 0;
}

bool rbuf_push(volatile rbuf_t *buf, uint8_t data)
{
  const uint8_t next = (buf->wr + 1) % RBUF_SIZE;
  if (next == buf->rd)
    return false;
  buf->buf[buf->wr] = data;
  buf->wr = next;
  return true;
}

bool rbuf_pop(volatile rbuf_t *buf, uint8_t *data)
{
  if (buf->wr == buf->rd)
    return false;
  *data = buf->buf[buf->rd];
  buf->rd = (buf->rd + 1) % RBUF_SIZE;
  return true;
}

bool rbuf_peek(const volatile rbuf_t *buf, uint8_t *data)
{
  if (buf->wr == buf->rd)
    return false;
  const uint8_t last = (RBUF_SIZE + buf->rd - 1) % RBUF_SIZE;
  *data = buf->buf[last];
  return true;
}
