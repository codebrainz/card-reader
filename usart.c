#include "usart.h"
#include "rbuf.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

static volatile rbuf_t rx_buf;
static volatile rbuf_t tx_buf;

void usart_init(usart_baud_t baud)
{
  rbuf_init(&rx_buf);           // init RX buffer
  rbuf_init(&tx_buf);           // init TX buffer
  UBRR0H = (uint16_t)baud >> 8; // set baud rate high byte
  UBRR0L = (uint16_t)baud;      // set baud rate low byte
  UCSR0C = 0x06;                // 8n1
  UCSR0B |= 1 << RXEN0;         // enable receiver
  UCSR0B |= 1 << TXEN0;         // enable transmitter
  UCSR0B |= 1 << RXCIE0;        // enable data received interrupt
  UCSR0B |= 1 << UDRIE0;        // enable transmit ready interrupt
}

void usart_putc(char ch)
{
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    while (!rbuf_push(&tx_buf, ch))
      ;
  }
}

void usart_puts(const char *str)
{
  for (const char *p = str; *p; p++)
    usart_putc(*p);
}

void usart_put_line(const char *str)
{
  usart_puts(str);
  usart_puts("\r\n");
}

bool usart_getc(char *ch)
{
  uint8_t byte = 0;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    if (!rbuf_pop(&rx_buf, &byte))
      return false;
  }
  *ch = byte;
  return true;
}

bool usart_peekc(char *ch)
{
  uint8_t byte = 0;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
  {
    if (!rbuf_peek(&rx_buf, &byte))
      return false;
  }
  *ch = byte;
  return true;
}

ISR(USART_RX_vect)
{
  const uint8_t data = UDR0;
  rbuf_push(&rx_buf, data);
}

ISR(USART_UDRE_vect)
{
  uint8_t data = 0;
  if (rbuf_pop(&tx_buf, &data))
    UDR0 = data;
}
