#ifndef USART_H
#define USART_H

#include <stdbool.h>

typedef enum
{
  USART_BAUD_2400 = 416,
  USART_BAUD_4800 = 207,
  USART_BAUD_9600 = 103,
  USART_BAUD_DEFAULT = USART_BAUD_9600,
  USART_BAUD_14K4 = 68,
  USART_BAUD_19K2 = 51,
  USART_BAUD_28K8 = 34,
  USART_BAUD_38K4 = 25,
  USART_BAUD_57K6 = 16,
  USART_BAUD_76K8 = 12,
  USART_BAUD_115K2 = 8,
  USART_BAUD_230K4 = 3,
  USART_BAUD_250K0 = USART_BAUD_230K4,
  USART_BAUD_0M5 = 1,
  USART_BAUD_1M0 = 0,
} usart_baud_t;

void usart_init(usart_baud_t baud);
void usart_putc(char ch);
void usart_puts(const char *str);
void usart_put_line(const char *str);
bool usart_getc(char *ch);
bool usart_peekc(char *ch);

#endif // USART_H
